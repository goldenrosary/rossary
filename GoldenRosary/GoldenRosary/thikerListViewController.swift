//
//  thikerListViewController.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 4/18/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import UIKit

protocol thikerListViewControllerDelegate {
    func didSelectTheker(thiker: ThikerObject)
}

class thikerListViewController: UIViewController ,  UITableViewDelegate , UITableViewDataSource {
    
    
    lazy var allAthkar = ThikerManager.shared.loadAllAthkar()
    
    var delegate: thikerListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table setting
    

    
    // Find Number of rows Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return allAthkar!.count
    }
    
    // content of cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AthekirType") as! ThikerTypeTableViewCell
        cell.thiker.text = allAthkar?[indexPath.row].text
       
        //print (UserDefaults.standard.integer(forKey: "currnetThekerIndex"))
        if(indexPath.row == UserDefaults.standard.integer(forKey: "currnetThekerIndex")){
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }



    // add checkmark on selected indexpath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.didSelectTheker(thiker: (allAthkar?[indexPath.row])!)
        
        let defaults = UserDefaults.standard
        defaults.set(indexPath.row, forKey: "currnetThekerIndex")
        defaults.synchronize()
        
        tableView.reloadData()
        
        
    }
  

    
    // remove checkmark on selected indexpath
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }
    
    
    
    @IBAction func closeButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
   
    
    
    

}
