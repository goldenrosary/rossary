//
//  HomeCollectionViewCell.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 4/3/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet var imgIcon: UIImageView!
}
