//
//  DetailUIView.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 4/8/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import UIKit

@IBDesignable class DetailUIView: UIView {

    var scale: CGFloat = 0.90
    
    var praiseCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    var praiseRadius: CGFloat {
        return min(bounds.size.width, bounds.size.height) / 2 * scale
    }
    func pathForCircleCenteredAtPoint(_ midPoint: CGPoint, withRadius radius: CGFloat) -> UIBezierPath
    {
        let path = UIBezierPath(
            arcCenter: midPoint,
            radius: radius,
            startAngle: 0.0,
            endAngle: CGFloat(2*M_PI),
            clockwise: false
        )
        UIColor.green.setFill()
        path.fill()
        path.lineWidth = 5.0
        return path
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        pathForCircleCenteredAtPoint(praiseCenter, withRadius: praiseRadius).stroke()
        
    }
    
    
    
   

}
