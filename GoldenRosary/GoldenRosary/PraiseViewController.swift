
//  PraiseViewController.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 4/3/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import UIKit
import KDCircularProgress
import AVFoundation
import CoreData

class PraiseViewController: UIViewController  , thikerListViewControllerDelegate {
    @IBOutlet weak var ThikerText: UIButton!
    
    @IBOutlet weak var numberOfThiker: UILabel!
    @IBOutlet weak var circularProgressView: UIView!
    
   
    @IBOutlet weak var circular: UILabel!
    
    var currentCount = 0.0
    var maxCount = 50.0
    var progress: KDCircularProgress!
    
    
    override func viewDidLoad() {
        
        //Thiker Action
        // return number of user count 
        restoreNumberOfThiker()
        //print ("\(currentCount)")
        super.viewDidLoad()
        progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: circularProgressView.frame.size.width, height: circularProgressView.frame.size.height))
        
        
        progress.startAngle = -90
        progress.progressThickness = 0.1
        progress.trackThickness = 0.6
        progress.clockwise = true
        progress.gradientRotateSpeed = 2
        progress.roundedCorners = true
        progress.glowMode = .forward
        progress.glowAmount = 0.9
        progress.set(colors: UIColor.cyan ,UIColor.white, UIColor.magenta, UIColor.white, UIColor.orange)

        progress.set(colors: UIColor.green , UIColor.red, UIColor.orange)
        circularProgressView.addSubview(progress)
        progress.autoresizingMask = [.flexibleBottomMargin , .flexibleWidth , .flexibleHeight , .flexibleTopMargin , .flexibleLeftMargin , .flexibleRightMargin]
        progress.translatesAutoresizingMaskIntoConstraints = true
        // Do any additional setup after loading the view.
        
        updateToCount(shouldVibrate: false)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func updateToCount(shouldVibrate:Bool) -> Void {
        
        numberOfThiker.text = String(Int(currentCount))
        let newAngleValue = newAngle()
        if currentCount == 0 {
            progress.angle = 1
        } else {
            progress.animate(toAngle: Double(newAngleValue), duration: 0.5, completion: { (finished) in})
        }
        
        //        }
        if shouldVibrate == true {
            AudioServicesPlayAlertSoundWithCompletion(kSystemSoundID_Vibrate, nil)
            
        }
        
        saveCurrentCount()
        
    }
    @IBAction func incearseButton(_ sender: increaseUIButton) {
        
        if currentCount < maxCount {
            currentCount += 1
            updateToCount(shouldVibrate: false)
        } else  {
            currentCount = 0
            updateToCount(shouldVibrate: true)
        }
        saveCurrentCount()
    }
    
    func saveCurrentCount()  {
        let defaults = UserDefaults.standard
        defaults.set(currentCount, forKey: "defultValue")
        defaults.synchronize()
    }
    
    func newAngle() -> Int {
        return Int(360 * (currentCount / maxCount))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func restoreNumberOfThiker() {
        //label text number of count
        numberOfThiker.text = String(UserDefaults.standard.double(forKey: "defultValue"))
        
        // for circle path progress
        currentCount = UserDefaults.standard.double(forKey: "defultValue")
        
    }
    
    // MARK: - Table View Thiker
    
    @IBAction func PerformTable(_ sender: UIButton) {
        
        createRowinDatabase(numberOfTasbeh: Int(currentCount) ,thikekerText: ThikerText.currentTitle! )
        currentCount = 0
        updateToCount(shouldVibrate: false)
        
        let listNav = self.storyboard?.instantiateViewController(withIdentifier: "ThikerListNavController") as! UINavigationController

        let listOfThiker = listNav.topViewController as! thikerListViewController
        

        listOfThiker.delegate = self
        
        listNav.modalTransitionStyle = .coverVertical
        
        self.present(listNav, animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)

//        
//        UIView.animate(withDuration: 01.3) {
//        
//            listOfThiker.view.alpha = 1.0
//            
//        }
        
        
        
    }
    
  
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: ListThekerViewControllerDelegate
    func didSelectTheker(thiker: ThikerObject) {
        
        ThikerText.setTitle(thiker.text , for: .normal)
        currentCount = 0
        
        currentCount = thiker.currentCount!
        updateToCount(shouldVibrate: false)
        print("should start with : \(thiker.text), \(ThikerText.currentTitle) ");
        
    }
    
    
    //MARK: - Insert into dataBase
    func createRowinDatabase(numberOfTasbeh: Int ,thikekerText: String ){
        // Do any additional setup after loading the view, typically from a nib.
        
        let statisticClassName:String = String(describing: Statistic.self)
        let statistic:Statistic = NSEntityDescription.insertNewObject(forEntityName: statisticClassName, into: DatabaseController.getContext()) as! Statistic
        
        let date = Date()
        
        statistic.thikerText = String(thikekerText)
        statistic.count = Int64(numberOfTasbeh)
        statistic.date  = date as NSDate!
        
        DatabaseController.saveContext()
        
        let fetchRequest:NSFetchRequest<Statistic> = Statistic.fetchRequest()
        
        do{
            let searchResults = try DatabaseController.getContext().fetch(fetchRequest)
            print("number of results: \(searchResults.count) , date: \(statistic.date) , Theker Text \(statistic.thikerText) count \(statistic.count) ")
            
            for result in searchResults as [Statistic]{
                print("\(result.count)  ")
            }
        }
        catch{
            print("Error: \(error)")
        }
        DatabaseController.getContext().delete(statistic)
    }
    
}
