//
//  Statistic+CoreDataProperties.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 5/2/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import Foundation
import CoreData


extension Statistic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Statistic> {
        return NSFetchRequest<Statistic>(entityName: "Statistic");
    }

    @NSManaged public var count: Int64
    @NSManaged public var date: NSDate?
    @NSManaged public var thikerText : String?
    @NSManaged public var newRelationship: KindOf?

}
