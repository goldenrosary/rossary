//
//  ViewController.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 4/3/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import UIKit
import CoreData

enum cellType:Int {
    case Praise = 0
    case Notifications = 1
    case PrayerTimes = 2
    case DailyReading = 3
}

class ViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {

    @IBOutlet var homeCollectionView: UICollectionView!
    
    var homeCells = [cellType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.homeCollectionView.reloadData()
        
        homeCells.append(.DailyReading)
        homeCells.append(.Praise)
        homeCells.append(.PrayerTimes)
        
        homeCells.append(.Notifications)
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK:- Collection view datasource 
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeCells.count
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        cell.backgroundColor = UIColor.red
       
        //cell.imageView.image = image
        
        switch homeCells[indexPath.row] {
        case .DailyReading:
            cell.lblTitle.text = "Praise"
            cell.imgIcon.image = UIImage(named: "praiseIcon")
            break
            
        default:
            cell.lblTitle.text = String(format:"rasha Cell %d" , indexPath.row)
            break
            
        }
        return cell
    }

  
    //MARK:- Collection view flow layout deleagte
    
        
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width - 24
        
        return CGSize(width: width/2.0, height:width/2.0 )
    }
    
    
    //MARK:- collection view delegate
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    print("didselect: \(indexPath.row)")
        if(indexPath.row == 0){
            let Praise = self.storyboard?.instantiateViewController(withIdentifier: "PriaseViewContollerID") as! PraiseViewController
            
            self.navigationController?.pushViewController(Praise, animated: true)
            
        }
    }

}

