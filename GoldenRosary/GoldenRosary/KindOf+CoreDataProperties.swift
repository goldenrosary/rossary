//
//  KindOf+CoreDataProperties.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 5/2/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import Foundation
import CoreData


extension KindOf {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<KindOf> {
        return NSFetchRequest<KindOf>(entityName: "KindOf");
    }

    @NSManaged public var type: String?
    @NSManaged public var newRelationship: Statistic?

}
