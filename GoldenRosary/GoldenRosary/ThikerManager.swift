//
//  ThikerManager.swift
//  GoldenRosary
//
//  Created by Rasha's Macbook on 5/8/1438 AH.
//  Copyright © 1438 Rasha's Macbook. All rights reserved.
//

import UIKit

class ThikerManager: NSObject {
    
    static let shared: ThikerManager = ThikerManager()
    private override init() {}
    var allAthkar:[ThikerObject]?
    
    func loadAllAthkar() -> [ThikerObject]? {
        
        if (allAthkar != nil) {
            return allAthkar
        }
        
        let thikerArr =  ["سبحان الله"
            , "الحمد لله"
            , "لا الله الا الله "
            , "الله اكبر"
            , "اللهم صلي على محمد"
            , "لاحول ولا قوه الا بالله" ]
        
        var result:[ThikerObject] = []
        var id = 0
        for thiker in thikerArr {
            let thikerObj = ThikerObject()
            thikerObj.id = id
            thikerObj.benefits = ""
            thikerObj.text = thiker
            thikerObj.load()
            id += 1
            result.append(thikerObj)
        }
        
        allAthkar = result
        
        return result
    }
    

}
