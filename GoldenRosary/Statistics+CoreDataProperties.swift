//
//  Statistics+CoreDataProperties.swift
//  
//
//  Created by Rasha's Macbook on 4/27/1438 AH.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Statistics {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Statistics> {
        return NSFetchRequest<Statistics>(entityName: "Statistics");
    }

    @NSManaged public var counter: Int16
    @NSManaged public var date: NSDate?

}
