//
//  Birthdate+CoreDataProperties.swift
//  
//
//  Created by Rasha's Macbook on 4/27/1438 AH.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Birthdate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Birthdate> {
        return NSFetchRequest<Birthdate>(entityName: "Birthdate");
    }

    @NSManaged public var day: NSDecimalNumber?
    @NSManaged public var month: NSDecimalNumber?
    @NSManaged public var year: NSDecimalNumber?

}
