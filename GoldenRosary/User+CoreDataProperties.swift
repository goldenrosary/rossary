//
//  User+CoreDataProperties.swift
//  
//
//  Created by Rasha's Macbook on 4/27/1438 AH.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var id: NSDecimalNumber?
    @NSManaged public var name: String?
    @NSManaged public var father: User?
    @NSManaged public var dateOfBirth: Birthdate?

}
